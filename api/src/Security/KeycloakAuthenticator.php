<?php

namespace App\Security;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

define("PUBLIC_KEY_HEADER", "-----BEGIN PUBLIC KEY-----\r\n");
define("PUBLIC_KEY_FOOTER", "\r\n-----END PUBLIC KEY-----");


class KeycloakAuthenticator extends AbstractGuardAuthenticator
{
    private $kcConfig;

    private $em;

    public function __construct(EntityManagerInterface $em, KeycloakConfig $kcConfig)
    {
        $this->em = $em;
       // $this->kcConfig = KeycloakConfig::loadFromEnv();
     //   $this->kcConfig = new KeycloakConfig();
        $this->kcConfig = $kcConfig;
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning false will cause this authenticator
     * to be skipped.
     * @param Request $request
     * @return bool
     */
    public function supports(Request $request)
    {
        return $request->headers->has('authorization');
    }

    /**
     * Called on every request. Return whatever credentials you want to
     * be passed to getUser() as $credentials .
     * => parse token from headers and check if valid
     * @param Request $request
     * @return array
     */
    public function getCredentials(Request $request)
    {
        $bearer = $request->headers->get('authorization');
        $rsaPublicKey = KeycloakProvider::getRealmPublicKey($this->kcConfig->getAuthServerUrl(), $this->kcConfig->getRealm());
        $claims = KeycloakProvider::getClaims($bearer, $rsaPublicKey);
        KeycloakProvider::validateClaims($this->kcConfig->getResource(), $this->kcConfig->getSecret(), $claims);
        return [
            'username' => $claims['preferred_username'],
        ];
    }

    /**
     * @param mixed $credentials
     * @param UserInterface $user
     * @return bool
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        return !is_null($user->getUsername());
    }


    /**
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey
     * @return Response|null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     * @return JsonResponse|Response|null
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = ['message' => strtr($exception->getMessageKey(), $exception->getMessageData())];
        return new JsonResponse($data, Response::HTTP_FORBIDDEN);
    }

    /**
     * Called when supports return false => if authorisation header not present
     * @param Request $request
     * @param AuthenticationException|null $authException
     * @return JsonResponse
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new JsonResponse(['message' => 'Bearer Required'], Response::HTTP_UNAUTHORIZED);
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        return new User($credentials['username'], null, []);
    }

    public function supportsRememberMe()
    {
    }

}
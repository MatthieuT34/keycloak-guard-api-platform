# Keycloak Guard API Platform

Use a Keycloak Guard to filter API Requests with API Platform APP

# Prerequisites

1) Have an `API Platform` project => https://api-platform.com

2) Have a keycloak server installed (You could find a docker-compose in auth/docker)

3) Have initialized Keycloak with: 
-  a realm (my-realm in this example)
-  At least a user created in this realm (username: toto, password: toto in the script)

You could find a script for initialization in the auth folder (auth/init-script.sh)

# Edit configs files

### config/package/Security.yml

Split `config/package/Security.yml` to `config/package/dev/Security.yml` and `config/package/prod/Security.yml`

#### file : **_config/package/prod/Security.yml_**

     security:
         # https://symfony.com/doc/current/security.html#where-do-users-come-from-user-providers
         providers:
             in_memory: { memory: null }
         firewalls:
             dev:
                 pattern: '^/(_(profiler|wdt)|css|images|js|docs)/'
                 security: false
     
             main:
                 guard:
                     authenticators:
                         - `App\Security\KeycloakAuthenticator`
     
                 # activate different ways to authenticate
                 # https://symfony.com/doc/current/security.html#firewalls-authentication
     
                 # https://symfony.com/doc/current/security/impersonating_user.html
                 # switch_user: true
     
         # Easy way to control access for large sections of your site
         # Note: Only the *first* access control that matches will be used
         access_control:
             # - { path: ^/admin, roles: ROLE_ADMIN }
             # - { path: ^/profile, roles: ROLE_USER }
             
 This file register a guard in `App\Security\KeycloakAuthenticator`
             
#### file : **_config/package/dev/Security.yml_**

    security:
        # https://symfony.com/doc/current/security.html#where-do-users-come-from-user-providers
        providers:
            in_memory: { memory: null }
        firewalls:
            dev:
                pattern: '^/(_(profiler|wdt)|css|images|js|docs)/'
                security: false
    
                # activate different ways to authenticate
                # https://symfony.com/doc/current/security.html#firewalls-authentication
    
                # https://symfony.com/doc/current/security/impersonating_user.html
                # switch_user: true
    
        # Easy way to control access for large sections of your site
        # Note: Only the *first* access control that matches will be used
        access_control:
            # - { path: ^/admin, roles: ROLE_ADMIN }
            # - { path: ^/profile, roles: ROLE_USER }
            
### config/services.yml

Edit `config/services.yml` to add keycloak variable

1) Add these following parameters in **parameters** section

         env(REALM): 'my-realm'
         env(AUTH_RESOURCE): 'api'
         env(AUTH_SECRET): ''
         env(AUTH_SERVER_URL): 'http://localhost:8080/auth'
         
   _These parameters are defaults value if env variable doesn't exist_
2) Add these following parameters in **__defaults**

        bind:
          $realm: '%env(resolve:REALM)%'
          $resource: '%env(resolve:AUTH_RESOURCE)%'
          $secret: '%env(resolve:AUTH_SECRET)%'
          $authServerUrl: '%env(resolve:AUTH_SERVER_URL)%'

3) Create a different .env for dev and prod

The .env is use both for dev and prod
The .env.dev is only use for dev and .env.prod only for .env.prod
The keycloak guard is only use in prod mode 

Put keycloak variables in .env.prod

    ###> keycloak ###
    
    AUTH_SERVER_URL=http://localhost:8080/auth
    REALM=my-realm
    AUTH_RESOURCE=api
    AUTH_SECRET=ec1e029f-d84c-4887-a966-6507ce7e6ed0
    

### Add keycloak Guard 

1) Create a Security directory at src/Security

2) Add `KeycloakConfig` Class in `src/Security`

          <?php
          
          
          namespace App\Security;
          
          class KeycloakConfig
          {
              private $realm;
              private $resource;
              private $secret;
              private $authServerUrl;
          
              /**
               * KeycloakConfig constructor.
               * @param $realm
               * @param $resource
               * @param $secret
               * @param $authServerUrl
               */
              public function __construct(string $realm,string $resource,string $secret,string $authServerUrl)
              {
                  $this->realm = $realm;
                  $this->resource = $resource;
                  $this->secret = $secret;
                  $this->authServerUrl = $authServerUrl;
              }
          
              /**
               * @return mixed
               */
              public function getRealm()
              {
                  return $this->realm;
              }
          
              /**
               * @return mixed
               */
              public function getResource()
              {
                  return $this->resource;
              }
          
              /**
               * @return mixed
               */
              public function getSecret()
              {
                  return $this->secret;
              }
          
              /**
               * @return mixed
               */
              public function getAuthServerUrl()
              {
                  return $this->authServerUrl;
              }
          
              public static function loadFromEnv()
              {
                  return new KeycloakConfig($_ENV['REALM'],$_ENV['AUTH_RESOURCE'], $_ENV['AUTH_SECRET'], $_ENV['AUTH_SERVER_URL']);
              }
          
          
          }
          
 3) Add `KeycloakProvider` lib in the same folder
  
        <?php
        
        namespace App\Security;
        
        use Firebase\JWT\ExpiredException;
        use Firebase\JWT\JWT;
        use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
        use \UnexpectedValueException;
        
        
        class KeycloakProvider
        {
        
            /**
             * @param string $authServerUrl
             * @param string $realm
             * @return string
             */
            public static function getRealmPublicKey(string $authServerUrl, string $realm)
            {
        
                $key = null;
        
                $url = $authServerUrl . "/realms/" . $realm;
        
                // Get cURL resource
                $curl = curl_init();
                // Set some options - we are passing in a useragent too here
                curl_setopt_array($curl, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => $url,
                    CURLOPT_USERAGENT => 'User Agent X'
                ));
                // Send the request & save response to $resp
                $resp = json_decode(curl_exec($curl));
                // Close request to clear up some resources
                curl_close($curl);
        
        
                if (property_exists($resp, 'public_key'))
                    $key = $resp->public_key;
        
                if (is_null($key)) {
                    throw new CustomUserMessageAuthenticationException('cannot get keycloak RSA public key', []);
                }
                return PUBLIC_KEY_HEADER . $key . PUBLIC_KEY_FOOTER;
            }
        
            /**
             * @param string $jwt
             * @param string $publicKey
             * @return array
             */
            public static function getClaims(string $jwt, string $publicKey)
            {
                try {
                    $tokenStr= null;
                    if (preg_match('/Bearer\s(\S+)/', $jwt, $matches)) {
                        $tokenStr = $matches[1];
                    }
                    $decoded = JWT::decode($tokenStr, $publicKey, array_keys(JWT::$supported_algs));
        
                } catch (SignatureInvalidException $e) {
                    throw new CustomUserMessageAuthenticationException('Token signature verification failed', []);
                } catch (BeforeValidException $e) {
                    throw new CustomUserMessageAuthenticationException('Token expiry time verification failed', []);
                } catch (ExpiredException $e) {
                    throw new CustomUserMessageAuthenticationException('Expired token', []);
                } catch (UnexpectedValueException  $e) {
                    throw new CustomUserMessageAuthenticationException('Invalid token', []);
                }
        
                return (array)$decoded;
            }
        
            public static function validateClaims(string $resource, string $secret, $claims){
                //TODO: Validate audience by resource
                //TODO: Validate secret of audience if possible
        
                return true;
            }
        }
        
  KeycloakProvider is a library for the authenticator
        
  4) Create the `KeycloakAuthenticator`
  
          <?php
          
          namespace App\Security;
          
          use Doctrine\ORM\EntityManagerInterface;
          use Symfony\Component\HttpFoundation\JsonResponse;
          use Symfony\Component\HttpFoundation\Request;
          use Symfony\Component\HttpFoundation\Response;
          use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
          use Symfony\Component\Security\Core\Exception\AuthenticationException;
          use Symfony\Component\Security\Core\User\User;
          use Symfony\Component\Security\Core\User\UserInterface;
          use Symfony\Component\Security\Core\User\UserProviderInterface;
          use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
          
          define("PUBLIC_KEY_HEADER", "-----BEGIN PUBLIC KEY-----\r\n");
          define("PUBLIC_KEY_FOOTER", "\r\n-----END PUBLIC KEY-----");
          
          
          class KeycloakAuthenticator extends AbstractGuardAuthenticator
          {
              private $kcConfig;
          
              private $em;
          
              public function __construct(EntityManagerInterface $em, KeycloakConfig $kcConfig)
              {
                  $this->em = $em;
                 // $this->kcConfig = KeycloakConfig::loadFromEnv();
               //   $this->kcConfig = new KeycloakConfig();
                  $this->kcConfig = $kcConfig;
              }
          
              /**
               * Called on every request to decide if this authenticator should be
               * used for the request. Returning false will cause this authenticator
               * to be skipped.
               * @param Request $request
               * @return bool
               */
              public function supports(Request $request)
              {
                  return $request->headers->has('authorization');
              }
          
              /**
               * Called on every request. Return whatever credentials you want to
               * be passed to getUser() as $credentials .
               * => parse token from headers and check if valid
               * @param Request $request
               * @return array
               */
              public function getCredentials(Request $request)
              {
                  $bearer = $request->headers->get('authorization');
                  $rsaPublicKey = KeycloakProvider::getRealmPublicKey($this->kcConfig->getAuthServerUrl(), $this->kcConfig->getRealm());
                  $claims = KeycloakProvider::getClaims($bearer, $rsaPublicKey);
                  KeycloakProvider::validateClaims($this->kcConfig->getResource(), $this->kcConfig->getSecret(), $claims);
                  return [
                      'username' => $claims['preferred_username'],
                  ];
              }
          
              /**
               * @param mixed $credentials
               * @param UserInterface $user
               * @return bool
               */
              public function checkCredentials($credentials, UserInterface $user)
              {
                  return !is_null($user->getUsername());
              }
          
          
              /**
               * @param Request $request
               * @param TokenInterface $token
               * @param string $providerKey
               * @return Response|null
               */
              public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
              {
                  return null;
              }
          
              /**
               * @param Request $request
               * @param AuthenticationException $exception
               * @return JsonResponse|Response|null
               */
              public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
              {
                  $data = ['message' => strtr($exception->getMessageKey(), $exception->getMessageData())];
                  return new JsonResponse($data, Response::HTTP_FORBIDDEN);
              }
          
              /**
               * Called when supports return false => if authorisation header not present
               * @param Request $request
               * @param AuthenticationException|null $authException
               * @return JsonResponse
               */
              public function start(Request $request, AuthenticationException $authException = null)
              {
                  return new JsonResponse(['message' => 'Bearer Required'], Response::HTTP_UNAUTHORIZED);
              }
          
              public function getUser($credentials, UserProviderInterface $userProvider)
              {
                  return new User($credentials['username'], null, []);
              }
          
              public function supportsRememberMe()
              {
              }
          
          }
      
  # Note 
  
  This documentation is incomplete. 
  
  Role is not current implemented

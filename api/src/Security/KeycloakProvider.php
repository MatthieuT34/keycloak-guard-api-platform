<?php

namespace App\Security;

use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use \UnexpectedValueException;


class KeycloakProvider
{

    /**
     * @param string $authServerUrl
     * @param string $realm
     * @return string
     */
    public static function getRealmPublicKey(string $authServerUrl, string $realm)
    {

        $key = null;

        $url = $authServerUrl . "/realms/" . $realm;

        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => 'User Agent X'
        ));
        // Send the request & save response to $resp
        $resp = json_decode(curl_exec($curl));
        // Close request to clear up some resources
        curl_close($curl);


        if (property_exists($resp, 'public_key'))
            $key = $resp->public_key;

        if (is_null($key)) {
            throw new CustomUserMessageAuthenticationException('cannot get keycloak RSA public key', []);
        }
        return PUBLIC_KEY_HEADER . $key . PUBLIC_KEY_FOOTER;
    }

    /**
     * @param string $jwt
     * @param string $publicKey
     * @return array
     */
    public static function getClaims(string $jwt, string $publicKey)
    {
        try {
            $tokenStr= null;
            if (preg_match('/Bearer\s(\S+)/', $jwt, $matches)) {
                $tokenStr = $matches[1];
            }
            $decoded = JWT::decode($tokenStr, $publicKey, array_keys(JWT::$supported_algs));

        } catch (SignatureInvalidException $e) {
            throw new CustomUserMessageAuthenticationException('Token signature verification failed', []);
        } catch (BeforeValidException $e) {
            throw new CustomUserMessageAuthenticationException('Token expiry time verification failed', []);
        } catch (ExpiredException $e) {
            throw new CustomUserMessageAuthenticationException('Expired token', []);
        } catch (UnexpectedValueException  $e) {
            throw new CustomUserMessageAuthenticationException('Invalid token', []);
        }

        return (array)$decoded;
    }

    public static function validateClaims(string $resource, string $secret, $claims){
        //TODO: Validate audience by resource
        //TODO: Validate secret of audience if possible

        return true;
    }
}
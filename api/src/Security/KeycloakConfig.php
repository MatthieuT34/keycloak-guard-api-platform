<?php


namespace App\Security;

class KeycloakConfig
{
    private $realm;
    private $resource;
    private $secret;
    private $authServerUrl;

    /**
     * KeycloakConfig constructor.
     * @param $realm
     * @param $resource
     * @param $secret
     * @param $authServerUrl
     */
    public function __construct(string $realm,string $resource,string $secret,string $authServerUrl)
    {
        $this->realm = $realm;
        $this->resource = $resource;
        $this->secret = $secret;
        $this->authServerUrl = $authServerUrl;
    }

    /**
     * @return mixed
     */
    public function getRealm()
    {
        return $this->realm;
    }

    /**
     * @return mixed
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * @return mixed
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * @return mixed
     */
    public function getAuthServerUrl()
    {
        return $this->authServerUrl;
    }

    public static function loadFromEnv()
    {
        return new KeycloakConfig($_ENV['REALM'],$_ENV['AUTH_RESOURCE'], $_ENV['AUTH_SECRET'], $_ENV['AUTH_SERVER_URL']);
    }


}
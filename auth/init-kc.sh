#!/usr/bin/env bash
#MAINTAINER "Matthieu TARTIERE"

# This script is usefull to define a default user
# Next step for authentication = connect to LDAP

if [ "$#" -ne 2 ]; then
    echo "Illegal number of parameters = Please specifiy KEYCLOAK_URL AND REDIRECT_URL"
    exit 2;
fi
KEYCLOAK_IP="$1"
REDIRECT_IP="$2"

REALM=my-realm
USERNAME=toto

# copy config in container
docker cp conf/user-config.json auth:/

#Auth with admin account
docker exec -ti auth /opt/jboss/keycloak/bin/kcadm.sh config credentials --server http://${KEYCLOAK_IP}/auth --realm master --user admin

#Create realm = my-realm
docker exec -ti auth /opt/jboss/keycloak/bin/kcadm.sh create realms -s realm="$REALM" -s enabled=true

#Create a new role for user
docker exec -ti auth /opt/jboss/keycloak/bin/kcadm.sh create roles -r "$REALM" -s name=user

#Create a new test user from config
docker exec -ti auth /opt/jboss/keycloak/bin/kcadm.sh create users -r "$REALM" -f /user-config.json

#Assign user to role

docker exec -ti auth /opt/jboss/keycloak/bin/kcadm.sh add-roles -r "$REALM" --uusername "$USERNAME" --rolename user

#Create new client for api
docker exec -ti auth /opt/jboss/keycloak/bin/kcadm.sh create clients -r "$REALM" -s clientId=api -s protocol=openid-connect -s publicClient=false -s bearerOnly=false -s secret=ec1e029f-d84c-4887-a966-6507ce7e6ed0

#Create new client for front
docker exec -ti auth /opt/jboss/keycloak/bin/kcadm.sh create clients -r "$REALM" -s clientId=front -s protocol=openid-connect -s publicClient=true -s "redirectUris=[\"http://${REDIRECT_IP}/*\"]"

